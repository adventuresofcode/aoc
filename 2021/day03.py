from Advent2021 import *

BinSeq = Tuple[str]

def parse_displacement(line: str) -> str:
    return line

in3: BinSeq = tuple(data(3, parse_displacement))

def day3_1(binsequence):
    gammarate = 0
    epsilonrate = 0
    mostcommon = ''
    leastcommon = ''
    strnumlen = len(binsequence[0])

    for c in range(strnumlen):
        iszero = 0
        isone = 0

        for l, s in enumerate(binsequence):
            a = s[c]
            #print(a)
            if a == '0':
                iszero += 1
            elif a == '1':
                isone += 1
            else:
                print(f"ERROR: {s[c]} -- {a}")
            # if l > 10:
            #     break

        if iszero > isone:
            # print(f"for {c} zeroes dominate {iszero} > {isone}")
            mostcommon += '0'
            leastcommon += '1'
        elif iszero < isone:
            # print(f"for {c} ones dominate {iszero} < {isone}")
            mostcommon += '1'
            leastcommon += '0'
        else:
             print("EQUALITY!")

    # print(f"mostcommon:  {mostcommon}\nleastcommon: {leastcommon}")

    # convert the array of chars to a binary number
    gammarate = int(mostcommon, 2)
    epsilonrate = int(leastcommon, 2)

    print(f"gammarate: {gammarate}  epsilonrate: {epsilonrate}  product: {gammarate * epsilonrate}")

    return gammarate * epsilonrate

def day3_2(binsequence):
    O2rate = 0
    CO2rate = 0
    strnumlen = len(binsequence[0])
    binlist = list(binsequence)

    # select bit in the number (c) starting from column 0
    # compute most and least common bits
    # select bit criteria (0 or 1) computed from
    #   most common value for O2
    #   least common value for CO2
    # remove all string that do not have this bit at the selected column
    # if one remains stop, otherwise c = c+1
    # we could iterate for both lists simulaltaneously or make a function
    for c in range(strnumlen):
        iszero = 0
        isone = 0
        mostcommon = ''
        leastcommon = ''

        # print(f"list length: {len(binlist)}")
        for l, s in enumerate(binlist):
            a = s[c]
            #print(a)
            if a == '0':
                iszero += 1
            elif a == '1':
                isone += 1
            # if l > 10:
            #     break

        if iszero > isone:
            # print(f"for {c} zeroes dominate {iszero} > {isone}")
            mostcommon = '0'
            leastcommon = '1'
        elif iszero < isone:
            # print(f"for {c} ones dominate {iszero} < {isone}")
            mostcommon = '1'
            leastcommon = '0'
        else:
            mostcommon = '1' # for O2
            leastcommon = '0'
            # print("EQUALITY!")

        # print(f"mostcommon:  {mostcommon}\nleastcommon: {leastcommon}")

        # filter numbers out of the string
        # instead of remove inplace from the list
        # just create a filtered list
        filtered = []
        for l, s in enumerate(binlist, start=1):
            # print(f"{s} -- {l} {len(binlist)}")
            if s[c] == mostcommon:
                filtered.append(s)

        # print(filtered)
        if len(filtered) == 1:
            # print(f"len: {len(filtered)} -- {filtered}")
            break

        binlist = filtered.copy()

    # print(f"resulting number: {filtered}")
    O2rate = int(filtered[0], 2)
    # print(f"O2rate: {O2rate}")

    # looking for the CO2rate now, leastcommon value
    strnumlen = len(binsequence[0])
    binlist = list(binsequence)

    for c in range(strnumlen):
        iszero = 0
        isone = 0
        mostcommon = ''
        leastcommon = ''

        # print(f"list length: {len(binlist)}")
        for l, s in enumerate(binlist):
            a = s[c]
            if a == '0':
                iszero += 1
            elif a == '1':
                isone += 1

        if iszero > isone:
            # print(f"for {c} zeroes dominate {iszero} > {isone}")
            mostcommon = '0'
            leastcommon = '1'
        elif iszero < isone:
            # print(f"for {c} ones dominate {iszero} < {isone}")
            mostcommon = '1'
            leastcommon = '0'
        else:
            mostcommon = '1' # for O2
            leastcommon = '0'
        #     print("EQUALITY!")

        # print(f"mostcommon:  {mostcommon}\nleastcommon: {leastcommon}")

        # filter numbers out of the string
        # instead of remove inplace from the list
        # just create a filtered list
        filtered = []
        for l, s in enumerate(binlist, start=1):
            # print(f"{s} -- {l} {len(binlist)}")
            if s[c] == leastcommon:
                filtered.append(s)

        # print(filtered)
        if len(filtered) == 1:
            # print(f"len: {len(filtered)} -- {filtered}")
            break

        binlist = filtered.copy()

    # print(f"resulting number: {filtered}")
    CO2rate = int(filtered[0], 2)
    # print(f"CO2rate: {CO2rate}")

    print(f"O2rate: {O2rate}  epsilonrate: {CO2rate}  product: {O2rate * CO2rate}")

    return O2rate * CO2rate


print(f"day3 part1 results:\n{day3_1(in3)}")
print(f"day3 part2 results:\n{day3_2(in3)}")
