from Advent2021 import *

DigitSegments = List[List[str]]

def digitsegments(line: str) -> DigitSegments:
    l, r = parse("{} | {}", line)
    ls = l.split()
    rs = r.split()
    for i, l in enumerate(ls):
        ls[i] = "".join(sorted(l))
    for i, l in enumerate(rs):
        rs[i] = "".join(sorted(l))

    return [ls, rs]

in8: DigitSegments = data(8, digitsegments)

def day8_1(segments):
    counter = 0
    for p in segments:
        for e in p[1]:
            l = len(e)
            if l == 2 or l == 3 or l == 4 or l == 7:
                counter += 1
    return counter

# function to return key for any value
def get_key(my_dict, val):
    for key, value in my_dict.items():
         if val == value:
             return key
    return ''

def day8_2B(segments):
    listofdicts = []
    for p in segments:
        d = {}
        for e in p[0]:
            d[e] = -1

        for k in d:
            if len(k) == 2:
                d[k] = 1
            elif len(k) == 3:
                d[k] = 7
            elif len(k) == 4:
                d[k] = 4
            elif len(k) == 7:
                d[k] = 8
            else:
                d[k] = -1

        for k in d:
            if len(k) == 5:
                # could be 2, 3 or 5
                # if you have the same two segments as 1 (c,f) it is a 3
                seg1 = get_key(d, 1)
                if k.find(seg1[0]) >= 0 and k.find(seg1[1]) >= 0:
                    d[k] = 3
                else:
                    # if we have 2 segments from 4 it is 2 otherwise 5 (with 3 segments)
                    count = 0
                    seg4 = get_key(d, 4)
                    for ll in seg4:
                        if k.find(ll) >= 0:
                            count += 1

                    if count == 2:
                        d[k] = 2
                    elif count == 3:
                        d[k] = 5
                    else:
                        print(f"ERROR: {count}")

        for k in d:
            if len(k) == 6:
                # could be 0, 6 or 9
                # 6 must have all segments from 5 and only 1 segment from 1
                # 9 must have all segments from 5 and all segments from 1
                # 0 is
                found = False
                seg5 = get_key(d, 5)
                seg1 = get_key(d, 1)
                for ll in seg5:
                    if k.find(ll) < 0:
                        d[k] = 0
                        found = True

                if not found:
                    count = 0
                    for ll in seg1:
                        if k.find(ll) >= 0:
                            count += 1

                    if count == 1:
                        d[k] = 6
                    elif count == 2:
                        d[k] = 9
                    else:
                        print(f"ERROROROROR: {count}")

        listofdicts.append(d)

    return listofdicts

def day8_2(segments):
    ld = day8_2B(segments)
    listofnumbers = []
    for j, p in enumerate(segments):

        number = 0
        for i in range(0, 4):
            digit = ld[j][p[1][i]]
            #print(f"{i}: {p[1][i]} = {digit}")
            number += pow(10, 3 - i) * digit

        listofnumbers.append(number)

    return sum(listofnumbers)

print(f"day8 part1 results:\n{day8_1(in8)}")
print(f"day8 part2 results:\n{day8_2(in8)}")
