from Advent2021 import *

FishesAge = List[int]

def fishes(line: str) -> FishesAge:
    s = line.split(sep=',')
    fishlist = [int(fish) for fish in s]
    return fishlist

# should take some time to make this cleaner
in6: FishesAge = data(6, fishes)[0]

#print(f"Initial population of {len(in6)} fishes:\n{in6}")

def day6_1(fishes):
    day = 0
    daymax = 80
    while day <= daymax:
        #fishes = list(map(lambda f: f - 1, fishes))
        #fishes = newfishes.copy() # not sure here w/r garbage collection
        #list(filter(lambda x: x < 0, f))
        for i, f in enumerate(fishes):
            if f < 0:
                fishes[i] = 6
                fishes.append(8)

            fishes[i] -= 1

        day += 1

    return len(fishes)

# the strategie is to handle pairs of fishes to represent the dynamics
# (age . number). Once age < 0, age = 6 and (8 . newnumber + number)
# another is to directly solve the differential equations... + linearity for the 300 initial conditions
def day6_2(fishes):
    fishespair = []
    for i in range(9):
        fishespair.append([i, 0])

    # fill in the initial structure
    for f in fishes:
        fishespair[f][1] += 1

    day = 0
    daymax = 256
    while day < daymax:
        #print(f"DAY: {day}")
        # make fishes age, translate down, -1 index first
        oldreproduce = fishespair[0][1]

        #print(f"before: {fishespair}")
        for i, f in enumerate(fishespair):
            #print(f"{i}: {f}")
            if i == 0:
                #print("continue")
                continue
            # differs from the part1 as we subtract first before performing arithmetic
            fishespair[i - 1][1] = fishespair[i][1]

        #print(f"after: {fishespair}")
        i = 6
        fishespair[i][1] += oldreproduce
        #print(f"{i}: {fishespair}")
        i = 8
        fishespair[i][1] = oldreproduce
        #print(f"{i}: push new generation: {fishespair}")
        #print(f"after: {fishespair}")

        #print(f"{fishespair}")
        day += 1

    fishessum = sum(np.array(fishespair)[:, 1])
    #print(f"Amount of fishes ({fishessum}):\n{fishespair}")
    return fishessum


print(f"day6 part1 results:\n{day6_1(in6)}")
print(f"day6 part2 results:\n{day6_2(in6)}")
