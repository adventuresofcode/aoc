from Advent2021 import *

import matplotlib.pyplot as plt

HotPath = Tuple[tuple, tuple]

def hotline(line: str) -> HotPath:
    x1, y1, x2, y2 = parse("{:d},{:d} -> {:d},{:d}", line)
    return ((x1, y1), (x2, y2))

in5: HotPath = tuple(data(5, hotline))

def day5_1(paths):
    hotmap = np.zeros((1000, 1000))
    # i = 0
    for path in paths:
        p1, p2 = path

        #if i < 5: print(f"path: {path}")
        # check for a vertical or horizontal line
        if p1[0] == p2[0]:
            # x is the same, vertical
            x = p1[0]
            y = np.arange(min(p1[1], p2[1]), max(p1[1], p2[1]) + 1)
            #if i < 5: print(f"range: ({x}, {y})")
            hotmap[x, y] += 1
        elif p1[1] == p2[1]:
            # y is the same, horizontal
            y = p1[1]
            x = np.arange(min(p1[0], p2[0]), max(p1[0], p2[0]) + 1)
            #if i < 5: print(f"range: ({x}, {y})")
            hotmap[x, y] += 1
        # else:
        #     print(f"ERROR invalid condition skip: {path}")
        # i += 1

    plt.imshow(hotmap)
    plt.savefig("mapday5A.png")

    mask = np.where(hotmap >= 2)
    crossings = len(mask[0])
    return crossings


def day5_2(paths):
    hotmap = np.zeros((1000, 1000))
    # i = 0
    for path in paths:
        p1, p2 = path

        #if i < 5: print(f"path: {path}")
        # check for a vertical or horizontal line
        if p1[0] == p2[0]:
            # x is the same, vertical
            x = p1[0]
            y = np.arange(min(p1[1], p2[1]), max(p1[1], p2[1]) + 1)
            #if i < 5: print(f"range: ({x}, {y})")
            hotmap[x, y] += 1
        elif p1[1] == p2[1]:
            # y is the same, horizontal
            y = p1[1]
            x = np.arange(min(p1[0], p2[0]), max(p1[0], p2[0]) + 1)
            #if i < 5: print(f"range: ({x}, {y})")
            hotmap[x, y] += 1
        else:
            #print(f"diagonal term: {path}")
            # check that the length is the same so that we are at 45 degrees
            # don't lose the direction !
            d1 = p2[0] - p1[0]
            d2 = p2[1] - p1[1]
            s1 = np.sign(d1)
            s2 = np.sign(d2)
            assert(abs(d1) == abs(d2))
            x = np.arange(min(p1[0], p2[0]), max(p1[0], p2[0]) + 1)
            y = np.arange(min(p1[1], p2[1]), max(p1[1], p2[1]) + 1)
            if s1 == -1:  x = np.flip(x)
            if s2 == -1:  y = np.flip(y)
            for x_, y_ in zip(x, y):
                #print(f"pos: ({x_}, {y_})")
                hotmap[x_, y_] += 1

        # i += 1

    plt.imshow(hotmap)
    plt.savefig("mapday5B.png")

    mask = np.where(hotmap >= 2)
    crossings = len(mask[0])
    assert(len(mask[0]) == len(mask[1]))
    return crossings


print(f"day5 part1 results:\n{day5_1(in5)}")
print(f"day5 part2 results:\n{day5_2(in5)}")
