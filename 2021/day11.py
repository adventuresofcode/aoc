from Advent2021 import *

import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.inf)

def characters(line: str):
    p = list(line)
    ip = [int(x) for x in p]
    return ip

in11 = data(11, characters)

def validbounds(p, xmin, xmax, ymin, ymax):
    if p[0] >= xmin and p[0] < xmax and p[1] >= ymin and p[1] < ymax:
        return p
    else:
        return []

def neighboursindexes(p, dim):
    nx = []
    ny = []
    k = list(p)
    k[0] -= 1
    k[1] -= 1
    for n in range(0, 3):
        for m in range(0, 3):
            #print(f"k: {k}")
            if validbounds(k, 0, dim, 0, dim):
                nx.append(k[0])
                ny.append(k[1])
            k[1] += 1
        k[0] += 1
        k[1] -= 3

    return nx, ny

def day11_1(lines):
    M = np.array(lines)
    F = np.ones(np.shape(M))
    totflashed = 0.0

    steps = 101
    for step in range(0, steps):
        #print(f"step: {step}\n{M}")
        #print(f"flashes: {np.sum(F == 0)}")
        totflashed += np.sum(F == 0)
        # reset masks
        F[:, :] = 1
        
        # compute a step
        M += 1
        # explode
        x, y = np.where(M > 9)
        
        while(len(x) > 0):
            for p in zip(x, y):
                #print(f"p: {p}: M[p] = {M[p]}")
                
                # flashing increase valid neighbours by 1
                if F[p] == 1:
                    F[p] = 0 # deactivate
                    nx, ny = neighboursindexes(p, 10)
                    for n in zip(nx, ny):
                        #print(f"n: {n}: M[n] = {M[n]}")
                        M[n] += 1
    
            x, y = np.where(M * F > 9)
    
        M = M * F

    return totflashed


def day11_2(lines):
    M = np.array(lines)
    F = np.ones(np.shape(M))
    totflashed = 0.0

    steps = 100000
    for step in range(0, steps):
        #print(f"step: {step}")
        #print(f"flashes: {np.sum(F == 0)}")
        totflashed += np.sum(F == 0)
        # reset masks
        F[:, :] = 1
        
        # compute a step
        M += 1
        # explode
        x, y = np.where(M > 9)
        
        while(len(x) > 0):
            for p in zip(x, y):
                #print(f"p: {p}: M[p] = {M[p]}")
                
                # flashing increase valid neighbours by 1
                if F[p] == 1:
                    F[p] = 0 # deactivate
                    nx, ny = neighboursindexes(p, 10)
                    for n in zip(nx, ny):
                        #print(f"n: {n}: M[n] = {M[n]}")
                        M[n] += 1
    
            x, y = np.where(M * F > 9)
    
        M = M * F
        if np.all(M == 0):
            return step + 1

    return -1

print(f"day11 part1 results:\n{day11_1(in11)}")
print(f"day11 part2 results:\n{day11_2(in11)}")
