from Advent2021 import *

in1: Tuple[int] = tuple(data(1, int))


def day1_1(nums):
    count = 0
    val = nums[0]
    for i in nums[1:]:
        if (i > val):
            count += 1
        val = i

    return count


def day1_2(nums):
    count = 0
    val = nums[0] + nums[1] + nums[2]
    for i, n in enumerate(nums[1:-2]):
        ss = nums[i+1] + nums[i+2] + nums[i+3]
        if (ss > val):
            count += 1
        val = ss

    return count


print(f"day1 part1 results:\n{day1_1(in1)}")
print(f"day1 part2 results:\n{day1_2(in1)}")
