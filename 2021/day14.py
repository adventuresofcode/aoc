from Advent2021 import *

import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.inf)

header = False
def characters(line: str):
    global header
    if not line:
        return []
    elif not header:
        header = True
        return list(line)
    else:
        s, r = parse("{} -> {}", line)
        return [list(s), r]

in14 = data(14, characters)


def day14_1(lines):
    template = lines[0]
    rules = lines[2:]

    steps = 10
    for step in range(1, steps + 1):
        i = 0
        inserted = 0
        newtemplate = template.copy()
        while(i < (len(template) - 1)):
            seq = template[i:i+2]
            for p, r in rules:
                if p == seq:
                    newtemplate.insert(i + 1 + inserted, r)
                    inserted += 1
                    #print(f"match: {p} = {seq} -> {r}: {template} -> {newtemplate}")
            i += 1
        template = newtemplate.copy()
        asstr = ''.join(template)
        if True:
            S = set(template)
            occ = []
            for c in S:
                occ.append([template.count(c), c])
            
            mx = max(occ)
            mn = min(occ)
            print(f"{mx[0] - mn[0]}")
    
    asstr = ''.join(template)
    #print(asstr)
    S = set(template)
    occ = []
    for c in S:
        occ.append([template.count(c), c])
    
    mx = max(occ)
    mn = min(occ)
    return mx[0] - mn[0]

def day14_2(lines):
    template = lines[0]
    rules = lines[2:]
    rules2 = {}
    rules_num = {}
    for r in rules:
        rr = ''.join(r[0])
        rules2[rr] = r[1]
        rules_num[rr] = 0

    bases = []
    for c, r in rules:
        bases.append(r)

    base = set(bases)
    
    # each rule has the following consequence:
    # increase the polymer template by adding two additional sequences in the
    # next iteration
    # AND adding an occurence of the inserted base
    base_num = {}
    for b in base:
        base_num[b] = 0

    # initial sequence
    for b in template:
        base_num[b] += 1

    template_string = ''.join(template)
    i = 0
    while(i < (len(template_string) - 1)):
        s = template_string[i:i+2]
        rules_num[s] += 1
        i += 1

    steps = 40
    for step in range(1, steps + 1):
        rules_next = rules_num.copy()
        rules_next = rules_next.fromkeys(rules_next, 0)
        for r, v in rules_num.items():
            if v > 0:
                p = rules2[r]
                a = r
                b = r
                a = a[:1] + p
                b = p + b[1:]
                #print(f"{r} -> {rules2[r]} produces:")
                #print(f"\t{a} -> {rules2[a]} and {b} -> {rules2[b]}")
                base_num[p] += v * 1
                rules_next[a] += v * 1
                rules_next[b] += v * 1
        rules_num = rules_next

    occlist = []
    for b, v in base_num.items():
        occlist.append(v)
        
    m = max(occlist)
    n = min(occlist)
    
    return m - n


print(f"day14 part1 results:\n{day14_1(in14[:])}")
print(f"day14 part2 results:\n{day14_2(in14[:])}")


# # trying to cheat my way through
# import scipy
# import scipy.optimize
# v = np.array([     5,      9,     18,     37,     60,    123,    256,    485,
#       984,   2010,   4088,   8314,  16715,  33832,  68483, 138585,
#    279423])
# lv = np.log(v)
# x = np.arange(0, 17)

# z = np.polyfit(x, lv, 1)
# p = np.poly1d(z)

# plt.plot(x, lv, p(x))

# plt.figure()
# plt.plot(x, v, np.exp(p(x)))

# scipy.optimize.curve_fit(lambda t,a,b: a*np.exp(b*t)+c,  x,  v,  p0=(4, 0.1, 5))
