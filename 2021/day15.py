from Advent2021 import *

import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.inf)

def characters(line: str):
    return [int(x) for x in list(line)]

in15 = data(15, characters)

# do a variational approach
# take an initial known path (one side for example)
# and then minimize progressively by keeping the boundaries and rules valid
def computerisk(path):
    s = 0
    for p in path[1:]:
        s += M[p]
    return s

def validpath(path):
    l = len(path)
    i = 0
    while(i < (l - 1)):
        p = path[i]
        q = path[i+1]
        d = (q[0] - p[0], q[1] - p[1])
        if d != (1, 0) and d != (-1, 0) and d != (0, 1) and d != (0, -1):
            return False
        i += 1
    return True


def day15_1(lines):
    M = np.array(lines)
    # initial cost
    cost = np.sum(M[:,0]) + np.sum(M[-1,:])
    path = []
    xmax = np.shape(M)[0]
    ymax = np.shape(M)[1]
    for x in range(0, xmax):
        p = (x, 0)
        path.append(p)
    for y in range(1, ymax):
        p = (xmax - 1, y);
        path.append(p)

    for p in path:
        M[p] = -10

    # create recursively paths that go to the bottom right
    # when reaching the end compute the risk, save path with minimum risks

    return 0

def day15_2(lines):

    return 0


print(f"day15 part1 results:\n{day15_1(in15[:])}")
print(f"day15 part2 results:\n{day15_2(in15[:])}")

