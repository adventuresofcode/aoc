from Advent2021 import *

import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.inf)

def characters(line: str):
    return line

in10 = data(10, characters)

def isopening(c):
    if c == "{" or c == "<" or c == "(" or c == "[":
        return True

    return False

def isclosing(c):
    if c == "}" or c == ">" or c == ")" or c == "]":
        return True

    return False

def reduceline(s):
    index = 0
    while (len(s) and len(s) > index):
        c = s[index]

        #print(f"line[{index}]={c}")
        # open keep
        # close, check -1 and pop
        if isopening(c):
            index += 1
            continue

        if c == ")":
            d = s[index - 1]
            if d == "(":
                s.pop(index)
                s.pop(index - 1)
                index -= 2
                #p = "".join(s)
                #print(f"str: {p}")
            else:
                #print(f"Corrupted: index[{index}]: {d}{c}")
                return c

        if c == "]":
            d = s[index - 1]
            if d == "[":
                s.pop(index)
                s.pop(index - 1)
                index -= 2
                #p = "".join(s)
                #print(f"str: {p}")
            else:
                #print(f"Corrupted: index[{index}]: {d}{c}")
                return c

        if c == ">":
            d = s[index - 1]
            if d == "<":
                s.pop(index)
                s.pop(index - 1)
                index -= 2
                #p = "".join(s)
                #print(f"str: {p}")
            else:
                #print(f"Corrupted: index[{index}]: {d}{c}")
                return c

        if c == "}":
            d = s[index - 1]
            if d == "{":
                s.pop(index)
                s.pop(index - 1)
                index -= 2
                #p = "".join(s)
                #print(f"str: {p}")
            else:
                #print(f"Corrupted: index[{index}]: {d}{c}")
                return c

    return s

def ratelist(v, l):
    if not l:
        return 0

    a = l.pop()
    if a == ")":
        v = 3
    elif a == "]":
        v = 57
    elif a == "}":
        v = 1197
    elif a == ">":
        v = 25137

    #print(f"v = {v}, a = '{a}' - l:{l}")
    return v + ratelist(v, l)

def ratecompletion(t, l):
    if not l:
        return t

    t = t * 5

    a = l.pop()
    if a == ")":
        v = 1
    elif a == "]":
        v = 2
    elif a == "}":
        v = 3
    elif a == ">":
        v = 4
    else:
        print("ERROR")

    #print(f"t: {t}, v: {v}, a: {a} and l: {l}")
    return ratecompletion(t + v, l)

def day10_1(lines):
    corrupt = []
    incomplete = []
    for i, line in enumerate(lines):
        s = list(line)
        l = reduceline(s)
        #print(f"result: {l}")
        if len(l) == 1:
            corrupt.append(l)
        elif len(l) == 0:
            pass
            #print(f"good line !")
        else:
            incomplete.append(l)
            #pass
            #print(f"incomplete line skip")


    #print(f"corrupted characters: {corrupt}")
    value = ratelist(0, corrupt)
    #print(f"corrupdate value: {value}")

    return value


def day10_2(lines):
    corrupt = []
    incomplete = []
    for i, line in enumerate(lines):
        s = list(line)
        l = reduceline(s)
        #print(f"result: {l}")
        if len(l) == 1:
            corrupt.append(l)
        elif len(l) == 0:
            pass
            #print(f"good line !")
        else:
            #incomplete.append(list(line))
            # save the already tentatively reduced line
            incomplete.append(l)
            #pass
            #print(f"incomplete line skip")

    # for each sequence of characters take the corresponding conjugate value
    scorelist = []
    for line in incomplete:
        closing = []
        line.reverse()
        for c in line:
            if c == '{':
                closing.append('}')
            elif c == '[':
                closing.append(']')
            elif c == '<':
                closing.append('>')
            elif c == '(':
                closing.append(')')
            else:
                print("ERROR")

        #print(f"closing line: {closing}")
        closing.reverse()
        score = ratecompletion(0, closing)
        scorelist.append(score)

    scorelist.sort()
    value = np.median(scorelist)

    return value

print(f"day10 part1 results:\n{day10_1(in10)}")
print(f"day10 part2 results:\n{day10_2(in10)}")
