from Advent2021 import *

CrabsPositions = Tuple[int]

def crabsparse(line: str) -> CrabsPositions:
    s = line.split(sep=',')
    crablist = [int(crab) for crab in s]
    return crablist


# should take some time to make this cleaner
in7: CrabsPositions = tuple(data(7, crabsparse)[0])

def day7_1(crabs):
    C = np.array(crabs)
    med = np.median(C)
    fuel = sum(abs(C - med))

    return fuel


def triangularnumber(n):
    return (n*(n + 1) / 2)


def day7_2(crabs):
    C = np.array(crabs)
    mean = np.mean(C)
    # used round() initially but given that 0 is a valid index, careful ! just truncate
    imean = int(mean)
    #print(f"rounding to: {imean}")
    dist = abs(C - imean)
    f = lambda x: triangularnumber(x)
    fuel = sum(f(dist))
    return fuel


print(f"day7 part1 results:\n{day7_1(in7)}")
print(f"day7 part2 results:\n{day7_2(in7)}")
