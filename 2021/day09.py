from Advent2021 import *

import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.inf)

HeightMap = List[List[str]]

def heightmap(line: str) -> HeightMap:
    n = [int(s) for s in line]
    return n

in9: HeightMap = data(9, heightmap)

xmax = 100
ymax = 100

def checkbounds(c):
    return (c[0] >= 0 and c[1] >= 0 and c[0] < xmax and c[1] < ymax)

def ismin(p, m):
    v = m[tuple(p)]

    c1 = p.copy(); c2 = p.copy(); c3 = p.copy(); c4 = p.copy()

    c1[0] -= 1
    c2[1] -= 1
    c3[0] += 1
    c4[1] += 1
    if ((checkbounds(c1) and m[tuple(c1)] < v)
        or (checkbounds(c2) and m[tuple(c2)] < v)
        or (checkbounds(c3) and m[tuple(c3)] < v)
        or (checkbounds(c4) and m[tuple(c4)] < v)
        or (m[tuple(c1)] == m[tuple(c1)] == m[tuple(c1)] == m[tuple(c1)] == v)):

        return False

    return True

def day9_1(hmap):
    # fill in a 2D array
    m = np.array(hmap, dtype=int)
    # make a mask of the local minima positions
    # make a function to look for neighbors
    h = np.zeros(m.shape, dtype=int)

    for x in range(m.shape[0]):
        for y in range(m.shape[1]):
            if ismin([x, y], m):
                h[x, y] = 1
                # print(f"{[x, y]} is a min with: {m[x, y]}")

    n = sum(h[h == 1])
    k = np.multiply(m, h)
    o = np.ndarray.sum(k)

    #print(f"number of minima: {n}, value of the minimas: {o}")

    plt.figure()
    plt.imshow(m)
    plt.colorbar()
    plt.savefig("mapday9A.png")

    plt.figure()
    plt.imshow(h)
    plt.colorbar()
    plt.savefig("mapday9Amask.png")

    plt.figure()
    plt.imshow(k)
    plt.colorbar()
    plt.savefig("mapday9Aprod.png")

    plt.close('all')

    return o + n

# got to do it recursively
# four direction boundary fill
# https://en.wikipedia.org/wiki/Flood_fill
def validinside(m, p, color):
    # out of range
    if not checkbounds(p):
        return False
    # border
    if m[tuple(p)] == 9:
        return False
    # already filled
    if m[tuple(p)] == color:
        return False
    return True

# we should color each basin with a different value
# and do the sorting filtering afterwards at the end
def fillbasin(m, p, color):
    if not validinside(m, p, color):
        return m

    m[tuple(p)] = color
    p1 = np.array(p) + [0, 1]
    p2 = np.array(p) + [0, -1]
    p3 = np.array(p) + [1, 0]
    p4 = np.array(p) + [-1, 0]
    m = fillbasin(m, p1, color)
    m = fillbasin(m, p2, color)
    m = fillbasin(m, p3, color)
    m = fillbasin(m, p4, color)
    return m

def day9_2(hmap):
    m = np.array(hmap, dtype=int)
    h = np.zeros(m.shape, dtype=int)
    # make a mask of the 9 map
    nines = [m == 9][0]
    nm = m * nines
    pl = []
    basins = [] # track start position and size

    for x in range(m.shape[0]):
        for y in range(m.shape[1]):
            if ismin([x, y], m):
                pl.append([x, y])

    # fill in the basin with value
    for color, p in enumerate(pl, start=10):
        nm = fillbasin(nm, p, color)

    # count domains size
    for color, p in enumerate(pl, start=10):
        s = len(nm[nm == color])
        basins.append([p, s])

    # adjust colors
    for p in pl:
        nm[tuple(p)] = -10

    plt.figure()
    plt.imshow(nm, cmap=plt.get_cmap("coolwarm"))
    plt.colorbar()
    plt.savefig("mapday9B.png")
    plt.close('all')

    nm[nm > 9] = 2
    nm[nm == -10] = 0
    nm[nm == 9] = -2

    plt.figure()
    plt.imshow(nm, cmap=plt.get_cmap("coolwarm"))
    plt.colorbar()
    plt.savefig("mapday9B2.png")
    plt.close('all')

    # sort the list by size of the basin, second element of each pair(list)
    r = sorted(basins, key = lambda x: x[1])

    mult = r[-1][1] * r[-2][1] * r[-3][1]
    return mult

print(f"day9 part1 results:\n{day9_1(in9)}")
print(f"day9 part2 results:\n{day9_2(in9)}")
