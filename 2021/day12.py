from Advent2021 import *

import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.inf)

def characters(line: str):
    i, o = parse("{}-{}", line)
    return [i, o]

in12 = data(12, characters)

# import dictionary for graph
from collections import defaultdict

# form https://www.python.org/doc/essays/graphs/

# function for adding edge to graph
graph = defaultdict(list)
def addEdge(graph,u,v):
    graph[u].append(v)
 
# definition of function
def generate_edges(graph):
    edges = []
 
    # for each node in graph
    for node in graph:
        # for each neighbour node of a single node
        for neighbour in graph[node]:
            # if edge exists then append
            edges.append((node, neighbour))
    return edges

def find_path(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return path
    for node in graph[start]:
        if node not in path:
            newpath = find_path(graph, node, end, path)
            if newpath:
                return newpath

def find_all_paths(graph, start, end, path =[]):
    path = path + [start]
    if start == end:
        return [path]
    paths = []
    for node in graph[start]:
        if node not in path:
            newpaths = find_all_paths(graph, node, end, path)
            for newpath in newpaths:
                paths.append(newpath)
    return paths

def find_all_specific_paths(graph, start, end, path = []):
    #print(f"path: {path}")
    path = path + [start]
    if start == end:
        return [path]
    paths = []
    for node in graph[start]:
        #print(f"node: {node}")
        # allow partial cycling (risk of infinite loop if two MAJ cave form a loop though)
        if (node not in path) or node.isupper():
            newpaths = find_all_specific_paths(graph, node, end, path)
            #print(f"new: {newpaths}")
            for newpath in newpaths:
                paths.append(newpath)
    return paths


def islowerduplicate(path):
    for p in path:
        if p.islower():
            if path.count(p) > 1:
                return True
    return False
        

def find_all_specific2_paths(graph, start, end, path = []):
    #print(f"path: {path}")
    path = path + [start]
    #graph_visited_node(start)
    if start == end:
        return [path]
    paths = []
    for node in graph[start]:
        #print(f"node: {node}")
        # allow partial cycling (risk of infinite loop if two MAJ cave form a loop though)
        if node not in path or node.isupper() or (node.islower() and node != 'start' and node != 'end' and not islowerduplicate(path)):
            newpaths = find_all_specific2_paths(graph, node, end, path)
            #print(f"new: {newpaths}")
            for newpath in newpaths:
                paths.append(newpath)
    return paths

# function to find the shortest path
def find_shortest_path(graph, start, end, path =[]):
    path = path + [start]
    if start == end:
        return path
    shortest = None
    for node in graph[start]:
        if node not in path:
            newpath = find_shortest_path(graph, node, end, path)
            if newpath:
                if not shortest or len(newpath) < len(shortest):
                    shortest = newpath
    return shortest

def day12_1(lines):
    s = 'start'
    e = 'end'
    
    graph = defaultdict(list)
    for i, o in lines:
        addEdge(graph, i, o)
        addEdge(graph, o, i)

    visited = {}
    all = find_all_specific_paths(graph, 'start', 'end')
    
    l = len(all)
    return l

def day12_2(lines):
    s = 'start'
    e = 'end'
    
    graph = defaultdict(list)
    for i, o in lines:
        addEdge(graph, i, o)
        addEdge(graph, o, i)

    all = find_all_specific2_paths(graph, 'start', 'end')
    
    l = len(all)
    return l

print(f"day12 part1 results:\n{day12_1(in12)}")
print(f"day12 part2 results:\n{day12_2(in12)}")
