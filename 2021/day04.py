from Advent2021 import *

BinSeq = Tuple[str]

def parse_bingo(line: str) -> str:
    return line

in4: BinSeq = tuple(data(4, parse_bingo))

def T(l): return np.transpose(l)

def day4_computeresult(X):
    return (X[0]*X[1]).sum() * X[2]

def day4_makebingolist(bingodata):
    bingolist = []
    numlines = len(bingodata)
    line = 1
    while line < numlines:
        bingo = []
        j = line+1
        for k in range(0, 5):
            #print(k)
            c = parse("{:d} {:d} {:d} {:d} {:d}", bingodata[j + k])
            bingo.append(c[0:5])

        bingolist.append(np.array(bingo))
        line = line+6

    return bingolist

def day4_1(bingodata):

    firstline = bingodata[0]

    # cast everything to numbers and put them in matrices
    numlist = firstline.split(sep=',')
    numlist = [int(s) for s in numlist]

    bingolist = day4_makebingolist(bingodata)
    # count the number of bingo boards we have
    bingoboards = len(bingolist) # should be 100

    # make a array of masks, make ones, put them to zero for matches
    # will help compute the remaining terms at the end, avoids negating the mask
    masks = []
    for i in range(0, bingoboards):
        masks.append(np.ones((5, 5)))

    for n, num in enumerate(numlist):
        # find num is a bingoboard and update the mask accordingly
        # finally check the masks for a full line or column
        for i in range(0, bingoboards):
            p = np.where(bingolist[i] == num)
            if len(p[0]) != 0:
                x = p[0][0]
                y = p[1][0]
                # print(f"found matching value {num}[{n}] on board i({i}) at position ({x}, {y})")
                masks[i][x][y] = 0
                if (len(p[0]) + len(p[1])) > 2:
                    print("ERROR Duplicates")
                    return

        # check if the bingo board is already valid
        for i in range(0, bingoboards):
            for l in range(0, 5):
                if masks[i][l, :].sum() == 0:
                    print(f"found one line ({l}) on board i({i})")
                    return day4_computeresult([bingolist[i], masks[i], num])
                elif masks[i][:, l].sum() == 0:
                    print(f"found one column ({l}) on board i({i})")
                    return day4_computeresult([bingolist[i], masks[i], num])


def day4_2(bingodata):

    firstline = bingodata[0]

    # cast everything to numbers and put them in matrices
    numlist = firstline.split(sep=',')
    numlist = [int(s) for s in numlist]

    bingolist = day4_makebingolist(bingodata)
    # count the number of bingo boards we have
    bingoboards = len(bingolist) # should be 100

    # make a array of masks, make ones, put them to zero for matches
    # will help compute the remaining terms at the end, avoids negating the mask
    masks = []
    for i in range(0, bingoboards):
        masks.append(np.ones((5, 5)))

    for n, num in enumerate(numlist):
        # find num is a bingoboard and update the mask accordingly
        # finally check the masks for a full line or column
        for i in range(0, len(bingolist)):
            p = np.where(bingolist[i] == num)
            if len(p[0]) != 0:
                x = p[0][0]
                y = p[1][0]
                # print(f"found matching value {num}[{n}] on board i({i}) at position ({x}, {y})")
                masks[i][x][y] = 0


        # check if the bingo board is already valid
        removeindex = []
        for i in range(0, len(bingolist)):
            for l in range(0, 5):
                if masks[i][l, :].sum() == 0:
                    #print(f"found one line ({l}) on board i({i})")
                    removeindex.append(i)
                elif masks[i][:, l].sum() == 0:
                    #print(f"found one column ({l}) on board i({i})")
                    removeindex.append(i)

        for c, l in enumerate(removeindex):
            if len(bingolist) == 1:
                #print(f"num: {num}\nboard:\n{bingolist[0]}\nmask:\n{masks[0]}")
                return day4_computeresult([bingolist[0], masks[0], num])

            del bingolist[l - c]
            del masks[l - c]
            # if len(bingolist) == 1:
            #     print(f"last one remaining ! Result coming soon.")

print(f"day4 part1 results:\n{day4_1(in4)}")
print(f"day4 part2 results:\n{day4_2(in4)}")
