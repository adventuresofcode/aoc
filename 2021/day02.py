# -*- coding: utf-8 -*-
"""
Created on Thu Dec  2 20:13:00 2021

@author: super
"""

from Advent2021 import *

Displacement = Tuple[str, int]

def parse_displacement(line: str) -> Displacement:
    s, i = parse('{} {:d}', line)
    return (s, i)

in2: Displacement = tuple(data(2, parse_displacement))

def day2_1(positions):
    depth = 0
    horizon = 0
    for s, i in positions:
        if s == 'forward':
            horizon += i
        elif s == 'down':
            depth += i
        elif s == 'up':
            depth -= i
        else:
            print('ERROR')

    print(f'horizon: {horizon}  depth: {depth}')
    return horizon * depth


def day2_2(positions):
    depth = 0
    horizon = 0
    aim = 0
    for s, i in positions:
        if s == 'forward':
            horizon += i
            depth += aim * i
        elif s == 'down':
            aim += i
        elif s == 'up':
            aim -= i
        else:
            print('ERROR')

    print(f'horizon: {horizon}  depth: {depth}')
    return horizon * depth


print(f"day2 part1 results:\n{day2_1(in2)}")
print(f"day2 part2 results:\n{day2_2(in2)}")
