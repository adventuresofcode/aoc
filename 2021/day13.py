from Advent2021 import *

import matplotlib.pyplot as plt
np.set_printoptions(threshold=np.inf)

def characters(line: str):
    if line.find('fold') >= 0:
        a, v = parse("fold along {}={}", line)
        return [a, v]
    elif len(line) == 0:
        pass
    else:
        i, o = parse("{},{}", line)
        return [i, o]

in13 = data(13, characters)


def day13_1(lines):
    foldlist = []

    while(lines[-1]):
        p = lines.pop()
        foldlist.append(p)
    lines.pop() # pop the None
    
    xmax = 0
    ymax = 0
    for p in lines:
        p0 = int(p[0])
        p1 = int(p[1])
        if p0 > xmax:
            xmax = p0
        if p1 > ymax:
            ymax = p1
            
    M = np.zeros((ymax + 1, xmax + 1))
    
    for p in lines:
        p0 = int(p[0])
        p1 = int(p[1])
        M[p1, p0] = 1
    
    foldlist.reverse()
    for f in foldlist:
        d = f[0]
        if d == 'x':
            v = int(f[1])
            yl, xl = np.shape(M)
            rl = xl - v - 1
            M[:, v] = -1
            S = M[:, v+1:]
            i = 0
            while(i < rl):
                M[:, v - (1 + i)] = M[:, v - (1 + i)] + S[:, i]
                i += 1
            M = M[:, 0:v]
        elif d == 'y':
            v = int(f[1])
            yl, xl = np.shape(M)
            rl = yl - v - 1
            M[v, :] = -1
            S = M[v+1:, :]
            i = 0
            while(i < rl):
                M[v - (1 + i), :] = M[v - (1 + i), :] + S[i, :]
                i += 1
            M = M[0:v, :]
        else:
            print("ERROR")
        break # quit after 1 fold

    M[M > 1] = 1
    s = np.sum(M)
    return s

def day13_2(lines):
    foldlist = []

    while(lines[-1]):
        p = lines.pop()
        foldlist.append(p)
    lines.pop() # pop the None
    
    xmax = 0
    ymax = 0
    for p in lines:
        p0 = int(p[0])
        p1 = int(p[1])
        if p0 > xmax:
            xmax = p0
        if p1 > ymax:
            ymax = p1
            
    M = np.zeros((ymax + 1, xmax + 1))
    
    for p in lines:
        p0 = int(p[0])
        p1 = int(p[1])
        M[p1, p0] = 1
    
    foldlist.reverse()
    for f in foldlist:
        d = f[0]
        if d == 'x':
            v = int(f[1])
            yl, xl = np.shape(M)
            rl = xl - v - 1
            M[:, v] = -1
            S = M[:, v+1:]
            i = 0
            while(i < rl):
                M[:, v - (1 + i)] = M[:, v - (1 + i)] + S[:, i]
                i += 1
            M = M[:, 0:v]
        elif d == 'y':
            v = int(f[1])
            yl, xl = np.shape(M)
            rl = yl - v - 1
            M[v, :] = -1
            S = M[v+1:, :]
            i = 0
            while(i < rl):
                M[v - (1 + i), :] = M[v - (1 + i), :] + S[i, :]
                i += 1
            M = M[0:v, :]
        else:
            print("ERROR")
        #break # quit after 1 fold

    M[M > 1] = 1
    #s = np.sum(M)
    #print(M)

    plt.figure()
    plt.imshow(M)
    plt.savefig("day13foldedcode.png")
    plt.close('all')
    
    # hardcode the answer as it needs human eyes.
    return "FJAHJGAH"

print(f"day13 part1 results:\n{day13_1(in13[:])}")
print(f"day13 part2 results:\n{day13_2(in13[:])}")
