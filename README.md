## Advent of Code

This is a personal archive about some of the challenges that were performed when time permitted.

Year 2021 was to practice some Python.

Year 2022 was to practice a bit of C++.

## Build
Move to the year folder and run `make`.
This we build all available challenges.

Optionaly, we can build a single challenge day with
`make day1` or any other day.

## Run

## Links
Website: [Advent of Code][def]

[def]: https://adventofcode.com/