#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

"""

from Advent2024 import *

AlphaNumString = Tuple[str]

def alphanum_line(line: str) -> str:
    return line

in1: AlphaNumString = tuple(data(1, alphanum_line))

def day1_1(lines: str) -> int:
    a = [x.split('   ') for x in in1]
    a = list(zip(*a))
    b = list(map(int, a[0]))
    c = list(map(int, a[1]))
    b.sort()
    c.sort()
    total = 0
    for x,y in zip(b, c):
        diff = abs(x - y)
        total += diff

    return total


def day1_2(lines: str) -> int:
    a = [x.split('   ') for x in in1]
    a = list(zip(*a))
    b = list(map(int, a[0]))
    c = list(map(int, a[1]))
    b.sort()
    c.sort()
    similarity = 0
    for x in b:
        accu = 0
        for y in c:
            if x == y:
                accu += 1

        similarity += x * accu

    return similarity


print(f"day1 part1 results:\n{day1_1(in1[:])}")
print(f"day1 part2 results:\n{day1_2(in1[:])}")
