#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  1 22:46:55 2024

@author: gabriel
"""
from collections import Counter

def read_and_parse_file(filename):
    """
    Reads a file with two columns of space-separated numbers and returns two sorted lists.
    """
    left_list = []
    right_list = []

    try:
        with open(filename, 'r') as file:
            for line in file:
                # Split the line into two parts based on whitespace
                parts = line.split()
                if len(parts) != 2:
                    raise ValueError("Each line must contain exactly two numbers separated by spaces.")

                # Convert the parts to integers and append to respective lists
                left_list.append(int(parts[0]))
                right_list.append(int(parts[1]))
    except FileNotFoundError:
        raise FileNotFoundError(f"The file {filename} does not exist.")
    except ValueError as e:
        raise ValueError(f"Error reading file {filename}: {e}")

    # Sort both lists
    left_list.sort()
    right_list.sort()

    return left_list, right_list

def calculate_total_distance(left_list, right_list):
    """
    Calculates the total distance between corresponding elements of two sorted lists.
    """
    if len(left_list) != len(right_list):
        raise ValueError("Both lists must have the same number of elements.")

    total_distance = 0
    for left, right in zip(left_list, right_list):
        distance = abs(left - right)
        total_distance += distance

    return total_distance

def read_and_parse_file2(filename):
    """
    Reads a file with two columns of space-separated numbers and returns two lists.
    """
    left_list = []
    right_list = []

    try:
        with open(filename, 'r') as file:
            for line in file:
                # Split the line into two parts based on whitespace
                parts = line.split()
                if len(parts) != 2:
                    raise ValueError("Each line must contain exactly two numbers separated by spaces.")

                # Convert the parts to integers and append to respective lists
                left_list.append(int(parts[0]))
                right_list.append(int(parts[1]))
    except FileNotFoundError:
        raise FileNotFoundError(f"The file {filename} does not exist.")
    except ValueError as e:
        raise ValueError(f"Error reading file {filename}: {e}")

    return left_list, right_list


def calculate_similarity_score2(left_list, right_list):
    """
    Calculates the similarity score based on the occurrences of numbers in the right list.
    """
    # Count occurrences of each number in the right list
    right_counter = Counter(right_list)

    # Calculate the similarity score
    similarity_score = 0
    for number in left_list:
        count = right_counter[number]
        similarity_score += number * count

    return similarity_score

def main(filename):
    """
    Main function to read the file, calculate the total distance, and print the result.
    """
    left_list, right_list = read_and_parse_file(filename)
    total_distance = calculate_total_distance(left_list, right_list)
    print(f"The total distance between the two lists is: {total_distance}")
    left_list, right_list = read_and_parse_file2(filename)
    similarity_score = calculate_similarity_score2(left_list, right_list)
    print(f"The similarity score between the two lists is: {similarity_score}")

# Example usage:
if __name__ == "__main__":
    filename = 'data/input1.txt'  # Replace with your actual filename
    main(filename)
