#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 21:43:04 2024

@author: gabriel
"""

def read_and_parse_file(filename):
    """
    Reads a file with reports of levels and returns a list of lists of integers.
    """
    reports = []

    try:
        with open(filename, 'r') as file:
            for line in file:
                # Split the line into parts based on whitespace and convert to integers
                levels = list(map(int, line.split()))
                reports.append(levels)
    except FileNotFoundError:
        raise FileNotFoundError(f"The file {filename} does not exist.")
    except ValueError as e:
        raise ValueError(f"Error reading file {filename}: {e}")

    return reports

def is_safe_report(levels):
    """
    Checks if a report (list of levels) is safe.
    """
    if len(levels) < 2:
        return True  # A single level is trivially safe

    # Determine if the report is increasing or decreasing
    increasing = decreasing = True

    for i in range(1, len(levels)):
        diff = levels[i] - levels[i - 1]
        if diff < -3 or diff > 3:
            return False  # Difference is too large or too small
        if diff > 0:
            decreasing = False
        if diff < 0:
            increasing = False
        if diff == 0:
            return False  # Adjacent levels are the same

    return increasing or decreasing

def is_safe_with_dampener(levels):
    """
    Checks if a report (list of levels) is safe with the help of the Problem Dampener.
    """
    if is_safe_report(levels):
        return True

    # Try removing each level and check if the report becomes safe
    for i in range(len(levels)):
        new_levels = levels[:i] + levels[i+1:]
        if is_safe_report(new_levels):
            return True

    return False

def count_safe_reports(reports):
    """
    Counts the number of safe reports.
    """
    safe_count = 0
    for report in reports:
        if is_safe_report(report):
            safe_count += 1
    return safe_count

def count_safe_reports_dampener(reports):
    """
    Counts the number of safe reports using the Problem Dampener.
    """
    safe_count = 0
    for report in reports:
        if is_safe_with_dampener(report):
            safe_count += 1
    return safe_count

def main(filename):
    """
    Main function to read the file, count the safe reports, and print the result.
    """
    reports = read_and_parse_file(filename)
    safe_count = count_safe_reports(reports)
    print(f"The number of safe reports is: {safe_count}")

def main2(filename):
    """
    Main function to read the file, count the safe reports with the Problem Dampener, and print the result.
    """
    reports = read_and_parse_file(filename)
    safe_count = count_safe_reports_dampener(reports)
    print(f"The number of safe reports with the Problem Dampener is: {safe_count}")

# Example usage:
if __name__ == "__main__":
    filename = 'data/input2.txt'  # Replace with your actual filename
    main(filename)
    main2(filename)
