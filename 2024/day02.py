#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

"""

from Advent2024 import *
import numpy as np

AlphaNumString = Tuple[str, ...]

def alphanum_line(line: str) -> str:
    return line

in2: AlphaNumString = tuple(data(2, alphanum_line))

def remove_elements_by_index(input_list: List[Any], indexes_to_remove: List[int]) -> List[Any]:
    """Remove elements from a list by their indexes."""
    return [i for j, i in enumerate(input_list) if j not in indexes_to_remove]


def increasedecreasesafe(reports: List[List[int,]]) -> int:
    safe = 0
    safeidx = []
    for idx, levels in enumerate(reports):
        diff_levels = np.diff(levels)
        # we have a vector decreasing or increasing
        seq = np.sign(diff_levels)
        seqsum = abs(sum(seq))
        seqabs = abs(diff_levels)
        if seqsum == len(diff_levels):
            if min(seqabs) < 1:
                continue
            elif max(seqabs) > 3:
                continue
            else:
                safe += 1
                safeidx.append(idx)

    remainingreports = remove_elements_by_index(reports, safeidx)
    return safe, remainingreports


def increasedecreaseoneoff(reports: List[List[int,]]) -> Tuple[int, List[List[int]]]:
    safe = 0
    for levels in reports:
        for jdx in range(len(levels)):
            levelsoff = levels.copy()
            levelsoff.pop(jdx)
            diff_levels = np.diff(levelsoff)
            # we have a vector decreasing or increasing
            seq = np.sign(diff_levels)
            seqsum = abs(sum(seq))
            seqabs = abs(diff_levels)
            if seqsum == len(diff_levels):
                if min(seqabs) < 1:
                    continue
                elif max(seqabs) > 3:
                    continue
                else:
                    safe += 1
                    break

    return safe

def day2_1(lines: str) -> int:
    reports = [list(map(int, line.split())) for line in lines]
    return increasedecreasesafe(reports)[0]

def day2_2(lines: str) -> int:
    reports = [list(map(int, line.split())) for line in lines]
    safe, remaining = increasedecreasesafe(reports)
    safe += increasedecreaseoneoff(remaining)
    return safe

print(f"day2 part1 results:\n{day2_1(in2[:])}")
print(f"day2 part2 results:\n{day2_2(in2[:])}")
