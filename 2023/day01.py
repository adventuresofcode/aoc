from Advent2023 import *

AlphaNumString = Tuple[str]

def alphanum_line(line: str) -> str:
    return line

in1: AlphaNumString = tuple(data(1, alphanum_line))

# we could have used RE and the ints() helper too
def day1_1(lines):
    numbers = [None] * len(lines)
    values = [None] * len(lines)
    for ln, line in enumerate(lines):
        numbers[ln] = []
        for idx in range(0, len(line)):
            if line[idx].isdigit():
                numbers[ln].append(line[idx])

    for ln, nums in enumerate(numbers):
        num_digits = len(nums)
        if num_digits == 0:
            print("ERROR")
        elif num_digits == 1:
            # print(f"Single digit: {lines[ln]}: {nums} => {int(nums[0] + nums[0])}")
            values[ln] = int(nums[0] + nums[0])
        elif num_digits >= 2:
            # print(f"Two+ digits: {lines[ln]}: {nums} => {int(nums[0] + nums[-1])}")
            values[ln] = int(nums[0] + nums[-1])

    values_sum = 0
    for value in values:
        values_sum += value

    return values_sum


def day1_2(lines):
    numbers = [None] * len(lines)
    values = [None] * len(lines)
    for ln, line in enumerate(lines):
        numbers[ln] = []
        for idx in range(0, len(line)):
            if line[idx].isdigit():
                numbers[ln].append(line[idx])
            elif line[idx:idx+3] == 'one':
                numbers[ln].append('1')
            elif line[idx:idx+3] == 'two':
                numbers[ln].append('2')
            elif line[idx:idx+5] == 'three':
                numbers[ln].append('3')
            elif line[idx:idx+4] == 'four':
                numbers[ln].append('4')
            elif line[idx:idx+4] == 'five':
                numbers[ln].append('5')
            elif line[idx:idx+3] == 'six':
                numbers[ln].append('6')
            elif line[idx:idx+5] == 'seven':
                numbers[ln].append('7')
            elif line[idx:idx+5] == 'eight':
                numbers[ln].append('8')
            elif line[idx:idx+4] == 'nine':
                numbers[ln].append('9')

    for ln, nums in enumerate(numbers):
        num_digits = len(nums)
        if num_digits == 0:
            print("ERROR")
        elif num_digits == 1:
            # print(f"Single digit: {lines[ln]}: {nums} => {int(nums[0] + nums[0])}")
            values[ln] = int(nums[0] + nums[0])
        elif num_digits >= 2:
            # print(f"Two+ digits: {lines[ln]}: {nums} => {int(nums[0] + nums[-1])}")
            values[ln] = int(nums[0] + nums[-1])

    values_sum = 0
    for value in values:
        values_sum += value

    return values_sum

print(f"day1 part1 results:\n{day1_1(in1)}")
print(f"day1 part2 results:\n{day1_2(in1)}")
