#from __future__ import annotations
from collections import Counter, defaultdict, namedtuple, deque
from itertools import permutations, combinations, product, chain
from functools import lru_cache, reduce
from typing import Dict, Tuple, Set, List, Iterator, Optional, Union, Iterable, Callable, Sequence, TypeVar, Any
from parse import parse, search, findall, with_pattern

import operator
import math
import ast
import sys
import re
import numpy as np

# Define type variables
T = TypeVar('T')
K = TypeVar('K')  # Key type for dictionaries
V = TypeVar('V')  # Value type for dictionaries

# Pre-compiled regular expressions for performance
int_pattern = re.compile(r'-?[0-9]+')

def data(day: int, parser: Callable[[str], T] = str, sep: str = '\n') -> list[T]:
    "Split the day's input file into sections separated by `sep`, and apply `parser` to each."
    with open(f'data/input{day}.txt') as file:
        sections = file.read().rstrip().split(sep)
    return [parser(section) for section in sections]

def do(day: int, *answers: int) -> Dict[int, int]:
    "E.g., do(3) returns {1: day3_1(in3), 2: day3_2(in3)}. Verifies `answers` if given."
    g = globals()
    got = {}
    for part in (1, 2):
        fname = f'day{day}_{part}'
        if fname in g: 
            result = g[fname](g[f'in{day}'])
            got[part] = result
            if len(answers) >= part: 
                assert result == answers[part - 1], (
                    f'{fname}(in{day}) got {result}; expected {answers[part - 1]}')
    return got

def quantify(iterable: Iterable, pred: Callable[[Any], bool] = bool) -> int:
    "Count the number of items in iterable for which pred is true."
    return sum(1 for item in iterable if pred(item))

def first(iterable: Iterable[T], default: Optional[T] = None) -> T:
    "Return first item in iterable, or default."
    return next(iter(iterable), default)

def rest(sequence: Sequence[T]) -> Sequence[T]: 
    return sequence[1:]

def multimap(items: Iterable[Tuple[K, V]]) -> dict[K, list[V]]:
    "Given (key, val) pairs, return {key: [val, ....], ...}."
    result = defaultdict(list)
    for (key, val) in items:
        result[key].append(val)
    return result

def prod(numbers: Iterable[float]) -> float:
    "The product of an iterable of numbers."
    return math.prod(numbers)

def ints(text: str) -> Tuple[int]:
    "Return a tuple of all the integers in text."
    return tuple(map(int, int_pattern.findall(text)))

def atoms(text: str, ignore: str = '', sep: Optional[str] = None) -> Tuple[Union[int, str]]:
    "Parse text into atoms (numbers or strs), possibly ignoring a regex."
    if ignore:
        text = re.sub(ignore, '', text)
    return tuple(map(atom, text.split(sep)))

def atom(text: str) -> Union[float, int, str]:
    "Parse text into a single float or int or str."
    try:
        val = float(text)
        return round(val) if round(val) == val else val
    except ValueError:
        return text

def dotproduct(A: Iterable[float], B: Iterable[float]) -> float: 
    return sum(a * b for a, b in zip(A, B))

def mapt(fn: Callable, *args: Iterable) -> Tuple:
    "map(fn, *args) and return the result as a tuple."
    return tuple(map(fn, *args))

cat = ''.join
flatten = chain.from_iterable
Char = str  # Type used to indicate a single character
