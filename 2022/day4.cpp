#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <fmt/core.h>
#include <fmt/format.h>
#include <vector>
#include <algorithm>
#include <map>
#include <valarray>
//#include <print> // for using println(); use fmt for now
#include <cassert>
#include <ranges>
#include <string_view>
#include <charconv>

using namespace std;
using namespace fmt;

// not yet released in stable libfmt neither in gcc 12
namespace fmt{
  template <typename... T>
  FMT_INLINE void println(std::FILE* f, format_string<T...> fmt, T&&... args) {
    return fmt::print(f, "{}\n", fmt::format(fmt, std::forward<T>(args)...));
  }
  template <typename... T>
  FMT_INLINE void println(format_string<T...> fmt, T &&...args) {
    return fmt::println(stdout, fmt, std::forward<T>(args)...);
  }
}; // namespace fmt


uint process_day4a(const auto lines)
{
  vector<vector<uint>> tasks {};
  for (auto line : lines)
    {
      if (line.empty())
	{
	  cerr << "Error line shouldn't be empty" << endl;
	  exit(EXIT_FAILURE);
	}

      //println("{}", line);
      std::string_view words(line);
      std::string_view delim{","};
      std::string_view delim2{"-"};
      vector<uint> task {};
      for (const auto word: std::views::split(words, delim))
	{
	  //std::cout << std::quoted(std::string_view{word.begin(), word.end()}) << ' ';
	  for (const auto num: std::views::split(word, delim2))
	    {
	      //std::cout << std::quoted(std::string_view{num.begin(), num.end()}) << ' ';
              uint task_val{};
              // auto result =
		std::from_chars(num.data(), num.data() + num.size(), task_val);
              //println("{}", task_val);
	      task.push_back(task_val);
            }
	}
      //cout << endl;
      tasks.push_back(task);
    }

  uint duplicate { 0 };
  for (auto task : tasks)
    {
      uint a {task[0]}, b {task[1]}, c {task[2]}, d {task[3]};
      //println("{} {} {} {}", a, b, c, d);
      if ((c >= a && d <= b) || (a >= c && b <= d))
	{
	  duplicate++;
	}
    }

  return duplicate;
}


uint process_day4b(const auto lines)
{
  vector<vector<uint>> tasks {};
  for (auto line : lines)
    {
      if (line.empty())
	{
	  cerr << "Error line shouldn't be empty" << endl;
	  exit(EXIT_FAILURE);
	}

      std::string_view words(line);
      std::string_view delim{","};
      std::string_view delim2{"-"};
      vector<uint> task {};
      for (const auto word: std::views::split(words, delim))
	{
	  //std::cout << std::quoted(std::string_view{word.begin(), word.end()}) << ' ';
	  for (const auto num: std::views::split(word, delim2))
	    {
	      //std::cout << std::quoted(std::string_view{num.begin(), num.end()}) << ' ';
              uint task_val{};
              /*auto result = */std::from_chars(num.data(), num.data() + num.size(), task_val);
              //println("{}", task_val);
	      task.push_back(task_val);
            }
	}
      // cout << endl;
      tasks.push_back(task);
    }

  uint duplicate { 0 };
  for (auto task : tasks)
    {
      uint a {task[0]}, b {task[1]}, c {task[2]}, d {task[3]};
      //println("{} {} {} {}", a, b, c, d);
      if (((b < c) && (b < d)) || ((a > c) && (a > d)))
	{
	  continue;
	}
      else
	{
	  duplicate++;
	}
    }

  return duplicate;
}


int main(int argc, char *argv[])
{
  if (argc != 2) {
    cerr << "This program expects one parameter that is the input file to read.\n";
    return 1;
  }

  ifstream input(argv[1]);
  if (!input.is_open()) {
    cerr << "Failed to open file.\n";
    return 1;
  }

  string line;
  vector<string> lines;
  while (getline(input, line))
    {
      lines.push_back(line);
    }

  uint day4a = process_day4a(lines);
  uint day4b = process_day4b(lines);

  println("{}", day4a);
  println("{}", day4b);
  return 0;
}
