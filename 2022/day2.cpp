#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <fmt/core.h>
#include <fmt/format.h>
#include <vector>
#include <algorithm>
//#include <print> // for using println(); use fmt for now
#include <cassert>

using namespace std;
using namespace fmt;

// not yet released in stable libfmt neither in gcc 12
namespace fmt{
  template <typename... T>
  FMT_INLINE void println(std::FILE* f, format_string<T...> fmt, T&&... args) {
    return fmt::print(f, "{}\n", fmt::format(fmt, std::forward<T>(args)...));
  }
  template <typename... T>
  FMT_INLINE void println(format_string<T...> fmt, T &&...args) {
    return fmt::println(stdout, fmt, std::forward<T>(args)...);
  }
}; // namespace fmt

uint compute_score(const char op, const char pl)
{
  uint score {};
  uint shape_op = op - 'A' + 1;
  uint shape_pl = pl - 'X' + 1;
  //print("shape: {} # {}\n", shape_op, shape_pl);
  if (shape_op == shape_pl) {
    // draw: 3 points
    score = 3;
    return score + shape_pl;
  }
  // else op != pl
  if ((shape_op == 1 && shape_pl == 3) || (shape_op == 2 && shape_pl == 1) ||
      (shape_op == 3 && shape_pl == 2)) {
    // opponent wins
    score = 0;
    return score + shape_pl;
  }
  // player wins
  score = 6;
  return score + shape_pl;
}


uint process_day2a(const auto& lines)
{
  uint total_score {};
  for (auto line : lines)
    {
      if (line.empty())
	{
	  cerr << "Error line shouldn't be empty" << endl;
	  exit(EXIT_FAILURE);
	}
      char opponent_move;
      char player_move;
      // scnlib or boost.spirit are good too!
      std::stringstream convertor(line);
      convertor >> opponent_move >> player_move;
      // sscanf(line.c_str(), "%c %c", &opponent_move, &player_move);

      uint round_score = compute_score(opponent_move, player_move);
      total_score += round_score;
      // print("{} {}: [{} {}]\n", opponent_move, player_move, round_score,
      //       total_score);
    }
  return total_score;
}

char guess_move(const char op, const char res)
{
  char guessed_move{};
  // draw
  if (res == 'Y')
    {
      guessed_move = 'X' + (op - 'A');
      return guessed_move;
    }
  // lose
  if (res == 'X')
    {
      if (op == 'A') // rock
	guessed_move = 'Z'; // scissors
      else if (op == 'B') // paper
	guessed_move = 'X'; // rock
      else {
        assert(op == 'C'); // scissors
        guessed_move = 'Y';
      }
      return guessed_move;
    }
  // win
  if (res == 'Z')
    {
      if (op == 'A') // rock
	guessed_move = 'Y'; // paper
      else if (op == 'B') // paper
	guessed_move = 'Z'; // scissors
      else {
        assert(op == 'C');  // scissors
        guessed_move = 'X'; // rock
        return guessed_move;
      }
      return guessed_move;
    }
  assert(0);
}


uint process_day2b(const auto &lines)
{
  uint total_score{};
  for (auto line : lines) {
    if (line.empty()) {
      cerr << "Error line shouldn't be empty" << endl;
      exit(EXIT_FAILURE);
    }
    char opponent_move;
    char outcome_result;
    // scnlib or boost.spirit are good too!
    stringstream convertor(line);
    convertor >> opponent_move >> outcome_result;

    char guessed_move = guess_move(opponent_move, outcome_result);
    uint round_score = compute_score(opponent_move, guessed_move);
    total_score += round_score;
    // print("{} {}: [{} {}]\n", opponent_move, player_move, round_score,
    //       total_score);
  }
  return total_score;
}

int main(int argc, char *argv[])
{
  if (argc != 2)
    {
      cerr << "This program expects one parameter that is the input file to read.\n";
      return 1;
    }

  ifstream input(argv[1]);
  if (!input.is_open()) {
    cerr << "Failed to open file.\n";
    return 1;
  }

  string line;
  vector<string> lines;
  while (getline(input, line))
    {
      lines.push_back(line);
    }

  uint day2a = process_day2a(lines);
  uint day2b = process_day2b(lines);

  println("{}", day2a);
  println("{}", day2b);
  return 0;
}
