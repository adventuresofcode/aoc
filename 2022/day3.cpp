#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <fmt/core.h>
#include <fmt/format.h>
#include <vector>
#include <algorithm>
#include <map>
#include <valarray>
//#include <print> // for using println(); use fmt for now
#include <cassert>

using namespace std;
using namespace fmt;

// not yet released in stable libfmt neither in gcc 12
namespace fmt{
  template <typename... T>
  FMT_INLINE void println(std::FILE* f, format_string<T...> fmt, T&&... args) {
    return fmt::print(f, "{}\n", fmt::format(fmt, std::forward<T>(args)...));
  }
  template <typename... T>
  FMT_INLINE void println(format_string<T...> fmt, T &&...args) {
    return fmt::println(stdout, fmt, std::forward<T>(args)...);
  }
}; // namespace fmt

// can be optimized out with additions and modular arithmetics
uint compute_score(const char op)
{
    if (islower(op)) {
      return op - 'a' + 1;
    } else {
      assert(isupper(op));
      return op - 'A' + 27;
    }
}


uint process_day3a(const auto lines)
{
  vector<char> priority_item {};
  for (auto line : lines)
    {
      if (line.empty())
	{
	  cerr << "Error line shouldn't be empty" << endl;
	  exit(EXIT_FAILURE);
	}

      string line_left;
      string line_right;

      // cut the string in two substrings
      auto line_len = line.length();
      auto line_seg = line_len / 2;

      line_left = line.substr(0, line_seg);
      line_right = line.substr(line_seg, line_len - 1);

      //println("{} [{} {}] ({} {})", line, line_left, line_right, line_len, line_seg);

      //stringstream ss(line_left);
      //sort(line_left.begin(), line_left.end());
      //sort(line_right.begin(), line_right.end());
      ranges::sort(line_left);
      ranges::sort(line_right);

      //auto last_left = unique(line_left.begin(), line_left.end());
      //auto last_right = unique(line_right.begin(), line_right.end());
      const auto [first_left, last_left] = ranges::unique(line_left);
      const auto [first_right, last_right] = ranges::unique(line_right);

      line_left.erase(first_left, last_left);
      line_right.erase(first_right, last_right);

      //println("{} [{} {}] ({} {})", line, line_left, line_right, line_len, line_seg);

      // println("{}   {}", line_left, line_right);
      //      line_left = line[0: line.length()/2];
      vector<char> res;
      set_intersection(line_left.begin(), line_left.end(),
		       line_right.begin(), line_right.end(),
		       back_inserter(res));

      assert(res.size() == 1);
      for (char l: res)
	{
          //println("{}", l);
	  priority_item.push_back(l);
	}
    }

  // we are done looping around
  uint total_score {};
  for (char l : priority_item)
    {
        auto score = compute_score(l);
        //println("{} [{}]", l, score);
	total_score += score;
    }

  return total_score;
}


uint process_day3b(const auto lines)
{
  vector<char> priority_item {};
  vector<string> rucksack_string {};
  for (auto line : lines)
    {
      if (line.empty())
	{
	  cerr << "Error line shouldn't be empty" << endl;
	  exit(EXIT_FAILURE);
	}

      ranges::sort(line);

      const auto [first, last] = ranges::unique(line);

      line.erase(first, last);

      rucksack_string.push_back(line);
    }

  while (!rucksack_string.empty())
    {
      string elf1 = rucksack_string.back();
      rucksack_string.pop_back();
      string elf2 = rucksack_string.back();
      rucksack_string.pop_back();
      string elf3 = rucksack_string.back();
      rucksack_string.pop_back();
      map<char, int> count;

      //println("--------");
      //println("{}", elf1);
      //println("{}", elf2);
      //println("{}", elf3);

      // intersect packs
      vector<char> res1;
      vector<char> res2;
      set_intersection(elf1.begin(), elf1.end(),
		       elf2.begin(), elf2.end(),
		       back_inserter(res1));

      set_intersection(elf3.begin(), elf3.end(),
		       elf2.begin(), elf2.end(),
		       back_inserter(res2));

      for (char l: res1)
	{
          //print("{}", l);
	}
      //print(" ");
      for (char l: res2)
	{
          //print("{}", l);
	}
      //cout << endl;

      // intersect the two vectors of chars now
      vector<char> last_inter;
      set_intersection(res1.begin(), res1.end(), res2.begin(), res2.end(), back_inserter(last_inter));

      assert(last_inter.size() == 1);
      for (auto l: last_inter)
	{
	  //print("{}", l);
	  priority_item.push_back(l);
	}
      //println("--------");
    }

  // we are done looping around
  uint total_score {};
  for (char l : priority_item)
    {
        auto score = compute_score(l);
        //println("{} [{}]", l, score);
	total_score += score;
    }
  return total_score;
}


int main(int argc, char *argv[])
{
  if (argc != 2) {
    cerr << "This program expects one parameter that is the input file to read.\n";
    return 1;
  }

  ifstream input(argv[1]);
  if (!input.is_open()) {
    cerr << "Failed to open file.\n";
    return 1;
  }

  string line;
  vector<string> lines;
  while (getline(input, line))
    {
      lines.push_back(line);
    }

  uint day3a = process_day3a(lines);
  uint day3b = process_day3b(lines);

  println("{}", day3a);
  println("{}", day3b);
  return 0;
}
