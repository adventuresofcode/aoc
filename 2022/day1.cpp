#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <fmt/core.h>
#include <fmt/format.h>
#include <vector>
#include <algorithm>

using namespace std;
using namespace fmt;


int main(int argc, char *argv[])
{
  if (argc != 2)
    {
      cerr << "This program expects one parameter that is the input file to read.\n";
      return 1;
    }

  ifstream input(argv[1]);
  if (!input.is_open()) {
    cerr << "Failed to open file.\n";
    return 1;
  }

  string line;
  vector<string> lines;
  while (getline(input, line))
    {
      lines.push_back(line);
    }

  // for (auto line: lines)
  //   {
  //     cout << line << endl;
  //     print("{}", line);
  //     navigate(result, line);
  //   }

  uint elves_calories_current = 0;
  vector<uint> elves_calories_list {};
  for (auto line: lines)
    {
      uint calories;
      stringstream ss;

      if (line.empty())
	{
	  elves_calories_list.push_back(elves_calories_current);
	  elves_calories_current = 0;
	  continue;
	}
      // if (0) {
      // 	ss << line;
      // 	ss >> calories;
      // }
      calories = stoi(line);
      elves_calories_current += calories;
      // print("l: {} : {} \n", line, calories);
    }

  sort(elves_calories_list.begin(), elves_calories_list.end(), greater<int>());
  //print("Number of elves: {}\n", elves_calories_list.size());
  // for (auto x: elves_calories_list)
  //   {
  //     cout << x << " ";
  //   }
  //  cout << endl;
  print("{}\n", elves_calories_list[0]); // Max calories on first elf:
  print("{}\n", elves_calories_list[0]
	+ elves_calories_list[1] + elves_calories_list[2]); // Max calories on top3 elves:

      return 0;
}
